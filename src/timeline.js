/**
 * Timeline class
 * @name Timeline
 */
export default class Timeline {

  /**
   * Class constructor, defining basic variables and rendering timeline
   * @param config 
   */
  constructor(config) {

    this.secondsInPeriod = 0;
    this.timePoints = {};

    this.config = config;
    this.render();
  }

  /**
   * Creating canvas object and appending to page
   */
  createCanvas() {
    this.canvas = document.createElement('canvas');
    this.canvas.innerText = 'Canvas is not supported in your browser!';
    this.canvas.setAttribute('width', this.width);
    this.canvas.setAttribute('height', this.height);
    this.canvas.style.backgroundColor = '#fff';
    document.body.appendChild(this.canvas);
    this.ctx = this.canvas.getContext('2d');
    return this;
  }

  /**
   * Main timeline
   */
  drawTimeline() {
    this.ctx.fillStyle = this.config.color.timeline;
    this.ctx.fillRect(0, 10, this.width, 10);
    return this;
  }

  /**
   * Period dividers on timeline 
   */
  drawPeriods() {
    let step = (this.width - 9) / 10;
    let drawPoint = step;
    this.ctx.fillStyle = '#000';
    for (let i = 0; i < 9; ++i) {
      this.ctx.fillRect(Math.round(drawPoint), 10, 1, 10);
      drawPoint += step;
      ++drawPoint;
    }
    return this;
  }

  /**
   * Initialize size of canvas object
   */
  initSize() {
    this.width = Math.max(200, document.body.offsetWidth);
    this.height = 30;
    return this;
  }

  /**
   * Return canvas object
   */
  getCanvas() {
    return this.canvas;
  }

  /**
   * Init function for window object
   * @param secondsInPeriod 
   */
  windowInit(secondsInPeriod) {

    // checking param for a zero or negative number
    if (secondsInPeriod <= 0) {
      this.errorMessage(`secondsInPeriod value should be greater than 0!`);
      return;
    }

    this.secondsInPeriod = secondsInPeriod;
    console.info(`The range of seconds in the period is set [${secondsInPeriod}]!`);
  }

  /**
   * Action function for window object
   * @param timeInSeconds 
   * @param team 
   */
  windowAddAction(timeInSeconds, team) {

    // checking if secondsInPeriod is set
    if (!this.secondsInPeriod) {
      this.errorMessage('Please run init function first and set seconds range in period!');
      return;
    }

    // checking if timeInSeconds in range
    if (timeInSeconds < 0 || timeInSeconds > this.secondsInPeriod * 10) {
      this.errorMessage('Your time in seconds is out of range!');
      return;
    }

    // validating team
    if (['HOME', 'AWAY'].indexOf(team) === -1) {
      this.errorMessage('Wrong team value! Team value should be only \'HOME\' or \'AWAY\'!');
      return;
    }

    // drawing point
    if (!this.timePoints.hasOwnProperty(timeInSeconds)) {
      this.timePoints[timeInSeconds] = {};
      this.timePoints[timeInSeconds][team] = 1;
      this.drawPoint(timeInSeconds, team);
    } else if (!this.timePoints[timeInSeconds].hasOwnProperty(team)) {
      this.timePoints[timeInSeconds][team] = 1;
      this.drawPoint(timeInSeconds, team);
    } else {
      this.timePoints[timeInSeconds][team] += 1;
      this.drawPoint(timeInSeconds, team, this.timePoints[timeInSeconds][team]);
    }
  }

  /**
   * Drawing timeline point
   * @param seconds 
   * @param team 
   * @param label 
   */
  drawPoint(seconds, team, label) {
    
    let y = (team === 'AWAY') ? 20 : 10;
    let x = this.width / (this.secondsInPeriod * 10) * seconds;

    this.ctx.beginPath();
    this.ctx.fillStyle = this.config.color[team.toLowerCase()];

    if (label !== undefined) {

      this.ctx.fillRect(x - 5, y - 5, 10, 10);

      this.ctx.fillStyle = '#fff';
      this.ctx.font = '8px Monaco';
      this.ctx.fillText(label, x - 3, y + 3);

    } else {

      this.ctx.fillStyle = this.config.color[team.toLowerCase()];
      this.ctx.ellipse(x, y, 3, 5, 0, 0, Math.PI * 2);
    }

    this.ctx.fill();
    this.ctx.closePath();
  }

  /**
   * Redrawing list of timeline points
   */
  redrawPoints() {
    for (let p in this.timePoints) {

      if (this.timePoints[p].hasOwnProperty('HOME')) {
        (this.timePoints[p]['HOME'] > 1)
          ? this.drawPoint(p, 'HOME', this.timePoints[p]['HOME'])
          : this.drawPoint(p, 'HOME');
      }

      if (this.timePoints[p].hasOwnProperty('AWAY')) {
        (this.timePoints[p]['AWAY'] > 1)
          ? this.drawPoint(p, 'AWAY', this.timePoints[p]['AWAY'])
          : this.drawPoint(p, 'AWAY');
      }
    }
  }

  /**
   * Rendering canvas timeline
   */
  render() {
    this.initSize()
      .createCanvas()
      .drawTimeline()
      .drawPeriods();
    return this;
  }

  /**
   * Display colored error message
   * @param message 
   */
  errorMessage(message) {
    console.error(
      `%c ${message} `,
      "background: red; color: white;"
    );
  }
}