import Timeline from './timeline.js';

/**
 * Small configuration for timeline object
 */
const config = {
  color: {
    timeline: '#CCC',
    home: '#12b6b8',
    away: '#c7413b'
  }
};

document.addEventListener("DOMContentLoaded", e => { 

  // init timeline object
  let timeLine = new Timeline(config);

  // init window function
  window.init = (lengthOfPeriodInSeconds) => {
    timeLine.windowInit(lengthOfPeriodInSeconds);
  }

  // add action window function
  window.addAction = (timeInSeconds, team) => {
    timeLine.windowAddAction(timeInSeconds, team);
  }

  // window resize action
  window.onresize = (e) => {
    try {
      document.body.removeChild(timeLine.getCanvas());
    } catch(e) { console.error(e); }
    timeLine.render().redrawPoints();
  };

});