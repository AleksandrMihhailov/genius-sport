const webpack = require('webpack');
const path = require('path');

module.exports = {
  
  devServer: {
    contentBase: [__dirname, 'src'].join(path.sep)
  },
  
  context: [__dirname, 'src'].join(path.sep),
  entry: {
    app: ['./app.js', './timeline.js']
  },
  
  output: {
    path: [__dirname, 'dist'].join(path.sep),
    filename: 'bundle.js'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [{
          loader: 'babel-loader',
          options: {
            presets: ['es2015']
          }
        }]
      }
    ]
  },

  devtool: "eval-source-map"
};