GeniusSport Test Task
=====================

Packed project in `dist` directory, just open `index.html` file in browser.

All source files in `src` directory.

---

### Development environment
Webpack and dev server installation
	
    npm install webpack webpack-dev-server -g

At first you need to install `yarn` package manager from [here](https://yarnpkg.com/lang/en/docs/install/).

Install webpack dependencies
        
    yarn install
---
Run development server with livereload

    yarn dev
---
Dev build

    yarn dev-build
---
Build for production

    yarn build